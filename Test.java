public class Test {

	// args is effectively final
	public static void main(String[] args) {
		int a = args.length;
		for (int i = 0; i < a; ++i) {
			a = m(i, a);
		}
	}

	// i is declared final
	// a is assigned to -> not effectively final
	static int m(final int i, int a) {
		a = i;
		int c;
		c = a;// only assigned once
		int d = a + i;
		return d;
	}

}
